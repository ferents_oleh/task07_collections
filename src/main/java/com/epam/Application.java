package com.epam;

public class Application {
    public static void main(String[] args) {
        BinaryTree<Integer, Integer> binaryTree = new BinaryTree<>(123, 454);
        binaryTree.put(1, 2);
        binaryTree.put(4, 3);
        binaryTree.put(2, 12);
        binaryTree.put(3, 4);
        binaryTree.put(222, 500);
        for (var v : binaryTree.entrySet()) {
            System.out.println(v.getKey() + " " + v.getValue());
        }
        binaryTree.remove(222);
        binaryTree.remove(2);
        binaryTree.remove(4);
        binaryTree.remove(1);
        binaryTree.remove(3);
        System.out.println("-----");
        for (var v : binaryTree.entrySet()) {
            System.out.println(v.getKey() + " " + v.getValue());
        }
        System.out.println(binaryTree.size());
        System.out.println(binaryTree.isEmpty());
    }
}
package com.epam;

import java.util.*;

public class BinaryTree<K extends Comparable<K>, V> implements Map<K, V> {
    private Node<K, V> root;

    private int size = 0;

    public BinaryTree() {}

    public BinaryTree(K key, V value) {
        root = new Node<>(key, value);
        size++;
    }

    static class Node<K, V> implements Map.Entry<K, V> {
        private K key;

        private V value;

        private Node<K, V> left;

        private Node<K, V> right;

        Node(K key, V value) {
            this.key = key;
            this.value = value;
            right = null;
            left = null;
        }

        @Override
        public K getKey() {
            return key;
        }

        @Override
        public V getValue() {
            return value;
        }

        @Override
        public V setValue(V value) {
            V oldValue = this.value;
            this.value = value;
            return oldValue;
        }
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean containsKey(Object key) {
        return false;
    }

    @Override
    public boolean containsValue(Object value) {
        return false;
    }

    @Override
    public V get(Object key) {
        Comparable<? super K> k = (Comparable<? super K>) key;
        Node<K, V> node = root;
        while (node != null) {
            int compareResult = k.compareTo(node.key);
            if (compareResult < 0) {
                node = node.left;
            } else if (compareResult > 0) {
                node = node.right;
            } else {
                return node.value;
            }
        }
        return null;
    }

    private Node<K, V> addRecursive(Node<K, V> node, K key, V value) {
        if (node == null) {
            node = new Node<>(key, value);
            return node;
        }

        int compareResult = key.compareTo(node.key);

        if (compareResult < 0) {
            node.left = addRecursive(node.left, key, value);
        } else if (compareResult > 0) {
            node.right = addRecursive(node.right, key, value);
        } else {
            return node;
        }
        return node;
    }

    @Override
    public V put(K key, V value) {
        root = addRecursive(root, key, value);
        size++;
        return root.value;
    }

    @Override
    public V remove(Object key) {
        K k = (K) key;
        Node<K, V> removeNode = root;
        Node<K, V> parent = root;
        Node<K, V> parentChild = null;
        Node<K, V> child;
        if (root == null) {
            return null;
        } else {
            int compareResult;
            while (true) {
                compareResult = k.compareTo(removeNode.key);
                if (compareResult < 0) {
                    if (removeNode.left == null) {
                        return null;
                    }
                    parent = removeNode;
                    removeNode = removeNode.left;
                } else if (compareResult > 0) {
                    if (removeNode.right == null) {
                        return null;
                    }
                    parent = removeNode;
                    removeNode = removeNode.right;
                } else {
                    break;
                }
            }
            if (removeNode.left == null && removeNode.right == null) {
                if (root == removeNode) {
                    root = null;
                } else if (parent.left == removeNode) {
                    parent.left = null;
                } else if (parent.right == removeNode) {
                    parent.right = null;
                }
            } else if (removeNode.left == null) {
                if (parent.left == removeNode) {
                    parent.left = removeNode.right;
                } else {
                    parent.right = removeNode.right;
                }
            } else if (removeNode.right == null) {
                if (parent.left == removeNode) {
                    parent.left = removeNode.left;
                } else {
                    parent.right = removeNode.left;
                }
            } else {
                child = removeNode.right;
                while (child.left != null) {
                    parentChild = child;
                    child = child.left;
                }
                if (parentChild != null) {
                    parentChild.left = child.right;
                } else {
                    removeNode.right = null;
                }
                child.left = removeNode.left;
                child.right = removeNode.right;
                if (removeNode != root) {
                    if (parent.left == removeNode) {
                        parent.left = child;
                    } else {
                        parent.right = child;
                    }
                } else {
                    root = child;
                }
            }
            size--;
            return removeNode.getValue();
        }
    }

    @Override
    public void putAll(Map<? extends K, ? extends V> m) {

    }

    @Override
    public void clear() {
        size = 0;
        root = null;
    }

    @Override
    public Set<K> keySet() {
        Set<K> keys = new HashSet<>();

        searchKey(root, keys);

        return keys;
    }

    private void searchKey (Node<K, V> root, Set<K> keySet) {
        if (root != null) {
            searchKey(root.left, keySet);
            keySet.add(root.getKey());
            searchKey(root.right, keySet);
        }
    }

    @Override
    public Collection<V> values() {
        Collection<V> collection = new ArrayList<>();
        searchValue(root, collection);
        return collection;
    }

    private void searchValue(Node<K, V> root, Collection<V> values) {
        if (root != null) {
            searchValue(root.left, values);
            values.add(root.getValue());
            searchValue(root.right, values);
        }
    }

    @Override
    public Set<Entry<K, V>> entrySet() {
        Set<Entry<K, V>> entrySet = new HashSet<>();
        searchNode(root, entrySet);
        return entrySet;
    }
    private void searchNode(Node<K, V> root, Set<Entry<K, V>> entrySet) {
        if (root != null) {
            searchNode(root.left, entrySet);
            entrySet.add(root);
            searchNode(root.right, entrySet);
        }
    }
}
